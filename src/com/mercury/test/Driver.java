package com.mercury.test;

import java.awt.AWTException;
import java.io.IOException;

import org.sikuli.script.FindFailed;

public class Driver {

	public static void main(String[] args) throws InterruptedException, AWTException, IOException, FindFailed {
		// TODO Auto-generated method stub

		//MethodRepository obj = new MethodRepository();
		MethodRepository.browserLaunch();
		MethodRepository.appLaunch();
		//MethodRepository.validLogin();
		//MethodRepository.verifiedValideLogin();
		//MethodRepository.departingFromSelection();
		//MethodRepository.clickOnContinue();
		//MethodRepository.browserClose();
		//MethodRepository.uploadPhoto();
		MethodRepository.loginUsingSikuli();
	}

}
