package com.mercury.test;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

public class MethodRepository {
	
	
	static WebDriver driver;
	
	
	public static void browserLaunch() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver","D:\\Tools_Selenium\\chromedriver.exe");
		 driver=new ChromeDriver();
		driver.manage().window().maximize();
		Thread.sleep(3000);
		
		}
	
	public static void uploadPhoto() throws InterruptedException, IOException
	{
		System.setProperty("webdriver.chrome.driver","D:\\Tools_Selenium\\chromedriver.exe");
		 driver=new ChromeDriver();
		driver.manage().window().maximize();
		
		driver.get("https://upload.photobox.com/en/#computer");
		Thread.sleep(5000);
		WebElement UName= driver.findElement(By.xpath("//button[@id = 'button_desktop']"));
		UName.click();
		
		Runtime.getRuntime().exec("D:\\Tools_Selenium\\uploadphoto.exe");
		
	}
	
	
	
	
	public static void appLaunch() throws InterruptedException{
		driver.get("http://newtours.demoaut.com/");
		Thread.sleep(3000);
	}
	
	
	public static void validLogin() throws InterruptedException, AWTException
	{
		
		WebElement UName= driver.findElement(By.name("userName"));
		
		UName.sendKeys("dasd");
		Thread.sleep(2000);
		
		WebElement pwd= driver.findElement(By.name("password"));
		pwd.sendKeys("dasd");
		Thread.sleep(2000);
		
		//WebElement login= driver.findElement(By.xpath("//input [@value='Login']"));
		//login.click();
		Robot login= new Robot();
		login.keyPress(KeyEvent.VK_TAB);
		login.keyRelease(KeyEvent.VK_TAB);
		login.keyPress(KeyEvent.VK_ENTER);
		login.keyRelease(KeyEvent.VK_ENTER);
		
	}
	
	public static void verifiedValideLogin()
	{
	
		String ExpectedVariable = "Find a Flight: Mercury Tours:";
		String ActualVariable= driver.getTitle();
		if (ExpectedVariable.equals(ActualVariable))
		{
			System.out.println("Pass");
		}
		
		else
			
		{
			System.out.println("Fail");
			
		}
	
	}
	
	
		public static void browserClose(){
		driver.close();
		
	}
	
		public  static void departingFromSelection()

		
		{
			WebElement Daparting = driver.findElement(By.name("fromPort"));
			
			//WebElement Daparting = driver.findElement(By.xpath("//select [@=name='fromPort']"));
			
			//Daparting.click();
			Select Departureitem = new Select(Daparting);
			//Departureitem.selectByIndex(3);
			//Departureitem.selectByValue("London");
			Departureitem.selectByVisibleText("Paris");
				
		}
		
		public  static void clickOnContinue ()
		{
		WebElement Continu= driver.findElement(By.name("findFlights"));
		Actions Click= new Actions(driver);
		Click.moveToElement(Continu).click().perform();
		//Click.click();
		}
		
	
		public static void loginUsingSikuli() throws InterruptedException, FindFailed
		{
			WebElement UName= driver.findElement(By.name("userName"));
			UName.sendKeys("dasd");
			Thread.sleep(2000);
			
			WebElement pwd= driver.findElement(By.name("password"));
			pwd.sendKeys("dasd");
			Thread.sleep(2000);
			
			Screen scr = new Screen();
			Pattern ptr = new Pattern("D:\\Tools_Selenium\\image\\signin.png");
			scr.click(ptr);
			
			
		}
		
		
		
		
}




